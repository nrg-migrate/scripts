#!/bin/bash

set -e
set +x

#GET THE JSESSION

#jsession=`XNATRestClient -u $user -p $pass -host $host -remote "REST/JSESSION" -m POST`



project=$1
session=$2
sessLabel=$3
host=$4
jsession=$5

#echo $jsession

echo -n "Processing $session"

connect_str="-user_session $jsession"

local_root_dir="/data/nil-bluearc/marcus/CNDA_ACCESSORIES/chpc_grid"
dir="${local_root_dir}/completed_jobs"


snapshot_dir="${local_root_dir}/completed_jobs/SNAPSHOTS"

freesurfer_out="${local_root_dir}/completed_jobs/XML"

datestamp=`date +%Y%m%d`


#Create the assessor

fs_id=`echo ${session}_freesurfer_51_${datestamp}`

#Clean up
\rm -rf ${dir}/$sessLabel/2011*/*average*
\rm -rf ${dir}/$sessLabel/RAW


pushd $freesurfer_out

scanid=`grep CopyDicomFiles $dir/$sessLabel/logs/${sessLabel}.err| awk '{split($0,a); print " -m " a[8]}'`

echo $sessLabel $scanid $fs_id




stats2xml_fs5.1.pl -p $project -x $session  $scanid -t Freesurfer -f $fs_id ${dir}/$sessLabel/2011*/${sessLabel}/stats


#Store the assessor


restPath="REST/experiments/${session}/assessors/${fs_id}"
echo $restPath



XNATRestClient -host $host $connect_str -local ${freesurfer_out}/${session}_freesurfer5.xml -remote $restPath -m PUT

popd

mkdir -p  ${snapshot_dir}/${sessLabel}
#Zip up the snapshots
pushd ${snapshot_dir}/${sessLabel}
mv ${dir}/$sessLabel/2011*/${sessLabel}/snapshots .
zip ${sessLabel} snapshots/*.gif
popd

#Upload the snapshots


XNATRestClient -host $host $connect_str -m PUT -remote "/REST/experiments/${session}/assessors/${fs_id}/resources/SNAPSHOTS"



XNATRestClient -host $host $connect_str -m PUT -remote "/REST/experiments/${session}/assessors/${fs_id}/resources/SNAPSHOTS/files?extract=true&content=VOLUME_SNAPSHOTS&label=VOLUME_SNAPSHOTS" -local ${snapshot_dir}/${sessLabel}/${sessLabel}.zip



#Zip up the logs
pushd ${dir}

zip -r ${freesurfer_out}/${sessLabel}_log ${sessLabel}/logs
popd

#Upload the logs


XNATRestClient -host $host $connect_str -m PUT -remote "/REST/experiments/${session}/assessors/${fs_id}/resources/LOG"



XNATRestClient -host $host $connect_str -m PUT -remote "/REST/experiments/${session}/assessors/${fs_id}/resources/LOG/files?extract=true&content=LOGS&label=LOGS" -local ${freesurfer_out}/${sessLabel}_log.zip


#Upload the freesurfer file

#Zip up the freesurfer folder

pushd $dir/${sessLabel}/2011*

zip -r ${freesurfer_out}/${sessLabel} ${sessLabel}/

popd

XNATRestClient -host $host $connect_str -m PUT -remote "/REST/experiments/${session}/assessors/${fs_id}/resources/DATA"



XNATRestClient -host $host $connect_str -m PUT -remote "/REST/experiments/${session}/assessors/${fs_id}/resources/DATA/files?extract=true&content=DATA" -local ${freesurfer_out}/${sessLabel}.zip



\rm ${freesurfer_out}/${sessLabel}_log.zip

\rm ${freesurfer_out}/${sessLabel}.zip

\rm ${snapshot_dir}/${sessLabel}/${sessLabel}.zip



echo "DONE"

exit 0
