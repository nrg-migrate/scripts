#!/usr/bin/python

import requests, json
from requests.auth import HTTPBasicAuth
import string
import random
from optparse import OptionParser
import time
import datetime
import sys
import csv

from pprint import pprint

parser = OptionParser()
# parser.add_option("-u", "--upload", dest="filename",
#                   help="write report to FILE", metavar="FILE")
#
# parser.add_option("-q", "--quiet",
#                   action="store_false", dest="verbose", default=True,
#                   help="don't print status messages to stdout")

parser.add_option("-o", "--host", action="store", dest="host_url", type="string", default="http://localxnat:8080/xnat/",
                  help="XNAT Server to use (eg- 'http://central.xnat.org/')")

parser.add_option("-u", "--username", action="store", dest="username", type="string", default="admin",
                  help="XNAT Username to use (Basic Auth)")

parser.add_option("-w", "--password", action="store", dest="password", default="admin", type="string",
                  help="XNAT Password to use (Basic Auth)")

parser.add_option("-p", "--projects",
                  action="store", dest="numprojects", type=int, default=0,
                  help="The number of projects to add")
parser.add_option("-s", "--subjects",
                  action="store", dest="numsubjects", type=int, default=0,
                  help="The number of subjects to add to each project")


parser.add_option("-z", "--projects-prefix", action="store", dest="projects_prefix", type="string", default="P", help="Prefix for generated project names")

parser.add_option("-a", "--subjects-prefix", action="store", dest="subjects_prefix", type="string",default="S", help="Prefix for generated subject names")

parser.add_option("-f", "--file",
                  action="store", type="string", dest="filename", default="none",
                  help="Specify a zip file of DICOM images to upload under a subject.  Will automatically create a new session under the project and subject for the images.")

parser.add_option("-v", "--verbose",
                  action="store_true", dest="verbose",
                  help="Turn verbose output on")

parser.add_option("-x", "--exit-on-error",
                  action="store_true", dest="exitonerror",
                  help="Exit the program if we run into an XNAT REST API error.")

# parser.add_option("-n", "-200svfile",
#                   action="store", dest="testnewuser",
#                   help="Exit the program if we run into an XNAT REST API error.")

parser.add_option("-n", "--csvfile",
                  action="store", type="string", dest="csvfilename", default="none",
                  help="Specify a CSV file containing usernames and configuration parameters")

parser.add_option("-c", "--createuser", action="store", dest="create_username", default="none", type="string",
                  help="Username to create")

parser.add_option("-X", "--proxy", action="store", dest="proxy", default=None, type="string",
                  help="HTTP proxy for call")

parser.add_option("-S", "--secure_proxy", action="store", dest="secure_proxy", default=None, type="string",
                  help="HTTPS proxy for call")

(options, args) = parser.parse_args()

# if (options.numprojects == 0):
#     parser.print_help()
#     quit()


# print options.filename
#
# quit()
username = options.username
password = options.password
proxies = {}
if not options.proxy is None:
    proxies["http"] = options.proxy
if not options.secure_proxy is None:
    proxies["https"] = options.secure_proxy

auther = HTTPBasicAuth(username, password)
BaseURL = options.host_url # "http://localhost:8080/xnat/"
data = {'format':'json'}
new_project_list = []
new_subject_list = []

# -------------- Classes ------------------


class XNATSubject:
    """A simple class for encapulating an XNAT subject"""
    handedness = "left";
    gender = "m";
    yob = "1970";
    def __init__(self):
        self.handedness = self.generate_handedness();
        self.gender = self.generate_gender();
        self.yob = self.generate_yob();
    def generate_gender(self):
        gi = random.randint(0, 10)
        gender = "male"
        if gi < 5: gender = "female"
        return gender

    def generate_handedness(self):
        gi = random.randint(0, 10)
        handedness = "right"
        if gi < 2: handedness = "left"
        return handedness

    def generate_yob(self):
        gi = random.randint(1940, 2005)
        return str(gi)



# ------------- Functions ------------------



def logoutput(str):
    if (options.verbose):
        ts = time.time()
        d = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
        print d + ": " + str


def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for x in range(size))


def addtestsubject(project_id, subjectid):
    "Add a test subject to a given project"
    url = BaseURL
    url += "REST/projects/" + project_id + "/subjects/"
    url += subjectid
    new_subject_list.append(subjectid)
    # Generate One Subject worth of Data
    subj = XNATSubject()


    logoutput("Creating Test Subject " + subjectid + " Gender: " + subj.gender + " Hand: " + subj.handedness + " YOB: " + subj.yob);

    data = {'session':id_generator(),
    'xnat:subjectData/demographics[@xsi:type=xnat:demographicData]/gender':subj.gender ,
    'xnat:subjectData/demographics[@xsi:type=xnat:demographicData]/yob':subj.yob ,
    'xnat:subjectData/demographics[@xsi:type=xnat:demographicData]/handedness':subj.handedness
    }
    ret = requests.put(url, params=data, auth=auther, proxies=proxies)
    if (ret.status_code  >= 400):
        print "Error adding a test subject: "
        print ret
        if (options.exitonerror == True):
            sys.exit()
    # print ret.text
    return 

def addtestproject(project_id):
    "Add a project to the system"
    new_id = project_id 
    url = BaseURL
    url += "REST/projects/" + new_id + ""
    new_project_list.append(new_id)
    logoutput("Creating Project ID: " + new_id)
    ret = requests.put(url, params=data, auth=auther, proxies=proxies)
    if (ret.status_code >= 400):
        print "Error adding a project: "
        print ret.text
        if options.exitonerror:
            quit(-1)

def upload_image_data(project_id, subject_id):
    "Handles the upload of DICOM data (generally a .zip file)"
    url = BaseURL
    url += "REST/services/import?dest=/archive/projects/" + project_id + ""
    logoutput("Uploading file " + options.filename + " to subject " + subject_id)
    f = open (options.filename)
    data = {'session':id_generator(), 'subject':subject_id}
    ret = requests.post(url=url, data = data,  auth=auther, files =  {'file':f}, proxies=proxies)
    if (ret.status_code  >= 400):
        print "Error Uploading DICOM data: "
        print ret
        if (options.exitonerror == True):
            sys.exit()


def createuser(username, password="password"):
    global auther
    json_template = '{ "login":"#USERNAME#","email":"#USERNAME#@bar.com","firstname":"#FIRSTNAME#","lastname":"#LASTNAME#","password":"#PASSWORD#","enabled":1,"verified":1,"userAuths": [{ "authId":"#USERNAME#","method":"localdb" }]}'
    url = BaseURL
    if not url.endswith("/"):
        url += "/"
    url += "data/user"
    tmpid = username
    firstname = "FIRST-" + tmpid
    lastname = "LAST-" + tmpid
    password = password
    auther = HTTPBasicAuth(options.username, options.password)
    json_data = json_template
    json_data = json_data.replace("#USERNAME#", username)
    json_data = json_data.replace("#FIRSTNAME#", firstname)
    json_data = json_data.replace("#LASTNAME#", lastname)
    json_data = json_data.replace("#PASSWORD#", password)
    logoutput(json_data)
    ret = requests.post(url=url, data=json_data,  auth=auther, proxies=proxies)
    if ret.status_code > 200:
        print "Error result from create user: "
        print ret
        print ret.text

# Read and process a CSV file that contains the information for a set of users, etc to create
# CSV format is USERNAME,<num_projects>,<num_subjects_per_project>,<DICOM filename>(optional)
def addnewuserscsv(csvfilename):
    global auther
    global options

    # json_data=open("./newuser.json").read()
    auther = HTTPBasicAuth(options.username, options.password)

    with open(csvfilename, 'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        for row in spamreader:
            print row[0]
            tmpid = row[0]
            createuser(tmpid);
            auther = HTTPBasicAuth(tmpid, "password")

            # Now, add some projects and subjects based on this user
            for i in range(int(row[1])):
                current_projid = options.projects_prefix + "-" + id_generator()
                addtestproject(current_projid)
                subj_id = current_projid + "-" + options.subjects_prefix + "-" + id_generator()
                for i in range(int(row[2])):
                    addtestsubject(current_projid, subj_id)
                    # Check to see if this row has a filename for DICOM images in it
                    if (len(row) > 3):
                        if (row[3] != ""):  
                            # upload a dicom data for this subject
                            options.filename = row[3]
                            upload_image_data(current_projid, subj_id)



 
# --------------------------- End Functions ----------------------------



ts = time.time()
d = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
logoutput(d + ": " + "Starting XNATLoad, using host " + BaseURL)

if (options.csvfilename != "none"):
    addnewuserscsv(options.csvfilename)
    sys.exit();

if (options.create_username != "none"):
    createuser(options.create_username)
    sys.exit()

logoutput("Adding " + str(options.numprojects) + " projects and " + str(options.numsubjects) + " subjects to each project.")
# Add a bunch of projects
if (options.numprojects > 0):
    for i in range(options.numprojects):
        addtestproject(options.projects_prefix + "-" + id_generator())


for i in new_project_list:
    # Add 10 test subjects to all the projects owned by admin
    # get the project ID from our previous call
    current_projid = i
    for z in range(options.numsubjects):
        subj_id = current_projid + "-" + options.subjects_prefix + "-" + id_generator()
        addtestsubject(current_projid, subj_id)
        # print "Filename is " + options.filename
        if (options.filename != "none"):
            upload_image_data(current_projid, subj_id)


# ---------------------------- Misc Stuff -------------------------------



# This is useful code if we want to get a list of all the current projects for specified user from the XNAT instance
#
# data = {'format':'json'}
# r = requests.get(base_url, params=data, auth=auther, proxies=proxies)
# result = json.loads(r.text)

# logoutput(result['ResultSet']['Result'][0]['description'])
# logoutput(result['ResultSet']['Result'][0]['URI'])
# logoutput(result['ResultSet']['Result'][0]['ID'])
# logoutput("Result Length is: ")
# resultlen = len(result['ResultSet']['Result'])
# projecturl = result['ResultSet']['Result'][0]['URI']
# projectid = result['ResultSet']['Result'][0]['ID']
# You can then loop over the returned resultset for whatever reason you'd like.
#quit()
# Take the URI and create 10 test subjects


# URL to upload a file of scan data
# http://localxnat:8080/xnat/REST/services/import?dest=/archive/projects/tp1&session=MR2&subject=tp1-ZXHZJK
#output r.json

"""
Here is a sample .csv file to use with this script:

USER-1,1,1,DICOM-sample1.zip
USER-2,2,2
USER-3,3,3,DICOM-sample1.zip
USER-4,3,3
USER-5,3,3
USER-6,3,3
USER-7,3,3
USER-8,3,3
USER-9,3,3
USER-10,3,3
USER-11,3,3
USER-12,3,3

"""