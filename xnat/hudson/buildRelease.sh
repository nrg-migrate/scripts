#!/bin/bash
CORE_USER=$1
CORE_REPO=$2
RELEASE_USER=$3
RELEASE_REPO=$4
PIPELINE_USER=$5
PIPELINE_REPO=$6
TEST_USER=$7
TEST_REPO=$8
ZIP_LOCATION=$9
VERSION=${10}
DIR="/opt/xnat_test"
TOMCAT="/usr/share/tomcat6"

die(){
  echo >&2 "$@"
  exit 1
}

[ "$#" -eq 10 ] || die "10 arguments required, $# provided (CORE_USER CORE_REPO RELEASE_USER RELEASE_REPO PIPELINE_USER PIPELINE_REPO TEST_USER TEST_REPO ZIP_LOCATION VERSION)"

if [ -d $DIR ] ; then
	\rm -r $DIR/build
	mkdir $DIR/build

	cd $DIR/build

	hg clone http://bitbucket.org/$CORE_USER/$CORE_REPO || die "Failed to clone core"
	\mv $CORE_REPO xdat_core

	hg clone http://bitbucket.org/$TEST_USER/$TEST_REPO || die "Failed to clone test"
	\mv $TEST_REPO xnat_test

	hg clone http://bitbucket.org/$RELEASE_USER/$RELEASE_REPO || die "Failed to clone xnat"
	\mv $RELEASE_REPO xnat

	hg clone http://bitbucket.org/$PIPELINE_USER/$PIPELINE_REPO pipeline || die "Failed to clone pipeline"
	\cp -R pipeline xnat/pipeline-installer || die "Failed to copy pipeline installer into xnat"

	mkdir xnat/deployments
	mkdir xnat/projects
	mkdir xnat/pipeline/logs

	##update xnat tag
	rm xnat/VERSION
	echo "$VERSION" > xnat/VERSION || die "Failed to version VERSION file."

	cd xnat/

	hg commit -u "admin@nrg.wustl.edu" -m "Modified VERSION file to reflect $VERSION" || die "Failed to commit VERSION file."

	hg tag --remove $VERSION
	hg tag -u "admin@nrg.wustl.edu" $VERSION || die "Failed to version VERSION file."

	hg commit -u "admin@nrg.wustl.edu" -m "Added tag for $VERSION release." || die "Failed to version mercurial hg."

	##update pipeline tag
	cd ../pipeline
	hg tag --remove $VERSION
	hg tag -u "admin@nrg.wustl.edu" $VERSION || die "Failed to version VERSION file."
	hg commit -u "admin@nrg.wustl.edu" -m "Added tag for $VERSION release." || die "Failed to version mercurial hg."

	##update core tag
	cd ../xdat_core
	hg tag --remove $VERSION
	hg tag -u "admin@nrg.wustl.edu" $VERSION || die "Failed to version VERSION file."
	hg commit -u "admin@nrg.wustl.edu" -m "Added tag for $VERSION release." || die "Failed to version mercurial hg."

	##update test tag
	cd ../xnat_test
	hg tag --remove $VERSION
	hg tag -u "admin@nrg.wustl.edu" $VERSION || die "Failed to version VERSION file."
	hg commit -u "admin@nrg.wustl.edu" -m "Added tag for $VERSION release." || die "Failed to version mercurial hg."

	cd ../

	zip -r xnat.zip xnat || die "Failed to zip."
	tar -cf xnat.tar xnat || die "Failed to tar."
	gzip xnat.tar || die "Failed to gz."

	cp xnat.zip $ZIP_LOCATION/xnat.zip || die "Failed to deploy zip."
	cp xnat.tar.gz $ZIP_LOCATION/xnat.tar.gz || die "Failed to deploy tar.gz."
else

die "directory: $DIR doesn't exist"

fi
