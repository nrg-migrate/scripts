#!/bin/bash
CORE_USER=$1
CORE_REPO=$2
RELEASE_USER=$3
RELEASE_REPO=$4
PIPELINE_USER=$5
PIPELINE_REPO=$6
TEST_USER=$7
TEST_REPO=$8
ZIP_LOCATION=$9
VERSION=${10}
DIR="/opt/xnat_test"
TOMCAT="/usr/share/tomcat6"

die(){
  echo >&2 "$@"
  exit 1
}

[ "$#" -eq 10 ] || die "10 arguments required, $# provided (CORE_USER CORE_REPO RELEASE_USER RELEASE_REPO PIPELINE_USER PIPELINE_REPO TEST_USER TEST_REPO ZIP_LOCATION VERSION)"

$DIR/buildRelease.sh $CORE_USER $CORE_REPO $RELEASE_USER $RELEASE_REPO $PIPELINE_USER $PIPELINE_REPO $TEST_USER $TEST_REPO $ZIP_LOCATION $VERSION ||  die "failed to build xnat zip"

$DIR/install_xnat_from_zip.sh $ZIP_LOCATION/xnat.zip ||  die "failed to build xnat from zip"

exit 0
