#!/bin/bash
ZIP_LOCATION=$1
DIR="/opt/xnat_test"
TOMCAT="/usr/share/tomcat6"
DB_USER="nrg"
DB_NAME="xnat_test"
JAVA_HOME="/usr/lib/jvm/java-1.7.0-oracle.x86_64"

die(){
  echo >&2 "$@"
  exit 1
}

[ "$#" -eq 1 ] || die "1 arguments required, $# provided (ZIP_LOCATION)"


if [ -d $DIR ] ; then
	cd $DIR

	unset CATALINA_HOME
	unset CATALINA_BASE
	unset CATALINA_PID
	unset CATALINA_TMPDIR

	export JAVA_HOME=$JAVA_HOME
	export PATH=$JAVA_HOME/bin:$PATH

	sudo /sbin/service tomcat6 stop

	sleep 5

	PID=`ps -eo pid,cmd | grep -m 1 'tomcat' | sed -e 's/^ *//' | cut -f1 -d" "`
	echo "PID: $PID"
	if [ $PID ]; then
	    echo "Killing PID : $PID"
	    kill -9 $PID
	fi

	echo "DROP/CREATE DB as $DB_USER"
	dropdb -U $DB_USER $DB_NAME
	createdb -U $DB_USER $DB_NAME ||  die "Failed to create $DB_NAME db"
	createlang -U $DB_USER plpgsql $DB_NAME


	cd $DIR
	\rm -r ~/.maven
	\rm -r xnat/

	\rm *.zip
	\rm *.tar
	\rm *.tar.gz


	\rm -r data
	mkdir data/
	mkdir data/archive
	mkdir data/build
	mkdir data/prearchive
	mkdir data/cache
	mkdir data/LoRes
	mkdir data/Thumbnail
	cd $DIR

	cp $ZIP_LOCATION $DIR/xnat.zip

	unzip xnat.zip ||  die "failed to unzip xnat.zip"

	\cp $DIR/build.properties xnat/ ||  die "failed to copy build.properties"

	if [ -f $DIR/security.xml ] ; then
		\cp $DIR/security.xml xnat/plugin-resources/project-skeletons/xnat/security/
	fi

	cd xnat

	chmod 777 bin/setup.sh 
	bin/setup.sh ||  die "setup.sh failed"
	psql -f $DIR/xnat/deployments/xnat/sql/xnat.sql -U $DB_USER $DB_NAME ||  die "update sql failed"
	cd deployments/xnat

	cd ../..
	cd $DIR/xnat/deployments/xnat
	../../bin/StoreXML -l security/security.xml -allowItemOverwrite true ||  die "Failed to store security.xml"
	../../bin/StoreXML -dir work/field_groups -u admin -p admin -allowItemOverwrite true ||  die "Failed to store field_groups"
	

	if [ -f $DIR/archive_specification.xml ] ; then
		../../bin/StoreXML -l $DIR/archive_specification.xml -u admin -p admin -allowItemOverwrite false ||  die "Failed to store archive_specification"
	fi

	
	if [ -f $DIR/test_user.xml ] ; then
		../../bin/StoreXML -l $DIR/test_user.xml -u admin -p admin -allowItemOverwrite false ||  die "Failed to store test_user"
	fi

	
	cd $DIR

	if [ -f $DIR/web-projectMerge.xml ] ; then
		\cp $DIR/web-projectMerge.xml xnat/projects/xnat/src/web-conf/
	fi

	

	if [ -f $DIR/site_description.vm ] ; then
		\cp $DIR/site_description.vm xnat/projects/xnat/src/templates/screens/
	fi

	cd $DIR/xnat
	bin/update.sh ||  die "update.sh failed"

	\rm -r $TOMCAT/webapps/xnat
	\rm $TOMCAT/webapps/xnat.war

	\cp $DIR/xnat/deployments/xnat/target/xnat.war $TOMCAT/webapps/xnat.war ||  die "Failed to deploy WAR"

	sudo /sbin/service tomcat6 start

	sleep 180

	echo ' LOGIN to http://hudson02.nrg.mir:8080/xnat for further testing'

	exit 0

else

die "directory: $DIR doesn't exist"

fi
