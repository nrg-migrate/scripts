import requests
import getpass
import os
from ConfigParser import RawConfigParser
from requests.auth import HTTPBasicAuth

__author__ = 'Rick Herrick <rick.herrick@wustl.edu'
__copyright__ = 'Copyright 2013, Washington University School of Medicine. All rights reserved.'
__license__ = 'Simplified BSD'
__maintainer__ = 'Rick Herrick'
__version__ = '1.6.2'
__email__ = 'rick.herrick@wustl.edu'
__status__ = 'Beta'


class XInit(object):
    """
        XInit
        Copyright (c) 2013, Washington University School of Medicine
        All Rights Reserved
        Released under the Simplified BSD License
        Application for initializing an XNAT server.
    """

    def __init__(self, server=None, user=None, password=None, config=None, target='default', proxy=None,
                 secure_proxy=None):
        """
            Parameters
            ----------
            server: string | None
                The server full URL (including port and XNAT instance name if necessary) e.g. http://central.xnat.org,
                http://localhost:8080/xnat_db. If None, the user will be prompted for it.
            user: string | None
                A valid login registered through the XNAT web interface. If None, the user will be prompted for it.
            password: string | None
                The user's password. If None, the user will be prompted for it.
            config: string
                Reads a config file in json to get the initialization parameters. In None, the user will be prompted for
                it.
            target: string | None
                Indicates the section of the config file to be used for initialization values. If no target is \
                specified, the section entitled "default" is used.
            proxy: string | None
                Indicates the full URL for an HTTP proxy server to be used for transactions with the specified XNAT
                server. If you need to specify a username and password for proxy access, prepend them to the hostname:
                http://user:pass@hostname:port
            secure_proxy: string | None
                Indicates the full URL for an HTTPS proxy server to be used for transactions with the specified XNAT
                server. If you need to specify a username and password for proxy access, prepend them to the hostname:
                https://user:pass@hostname:port

        """
        if server is None:
            self._server = raw_input('Server: ')
        else:
            self._server = server

        if user is None:
            user = raw_input('User: ')

        if password is None:
            password = getpass.getpass()

        if config is None:
            config = raw_input('Location of configuration file: ')

        self._user = user
        self._password = password
        self._config = config
        self._target = target

        self._parser = RawConfigParser()
        self._parser.optionxform = str
        self._parser.read(self._config)

        self._proxies = {}
        self.__set_proxy("http", proxy)
        self.__set_proxy("https", secure_proxy)

    def __set_proxy(self, protocol, proxy=None):
        if proxy is None:
            proxy = os.environ.get(protocol + "_proxy")
        if proxy is not None:
            self._proxies[protocol] = proxy

    def __get_service_url(self):
        return self._server + '/data/services/settings/initialize'

    def __convert_config_to_dict(self):
        parameters = dict(self._parser.items(self._target))
        return parameters

    def process(self):
        parameters = self.__convert_config_to_dict()
        auth = HTTPBasicAuth(self._user, self._password)
        response = requests.put(self.__get_service_url(), data=parameters, auth=auth, proxies=self._proxies)
        return {'status': response.status_code, 'reason': response.reason, 'text': response.text}