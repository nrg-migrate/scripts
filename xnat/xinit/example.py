#!/usr/bin/python
from xinit.XInit import XInit
from optparse import OptionParser

__author__ = 'rherri01'

parser = OptionParser()
parser.add_option("-o", "--host", action="store", dest="host_url", type="string", default="http://localhost:8080/xnat/",
                  help="XNAT Server to use (eg- 'http://central.xnat.org/')")

parser.add_option("-u", "--username", action="store", dest="username", type="string", default="admin",
                  help="XNAT Username to use (Basic Auth)")

parser.add_option("-w", "--password", action="store", dest="password", default="admin", type="string",
                  help="XNAT Password to use (Basic Auth)")

parser.add_option("-c", "--config", action="store", dest="config", type="string", default="example.ini",
                  help="Configuration file to load (defaults to example.ini)")

parser.add_option("-t", "--target", action="store", dest="target", type="string", default="default",
                  help="Section of the configuration file to be loaded (defaults to default)")

parser.add_option("-x", "--proxy", action="store", dest="proxy", type="string",
                  help="HTTP proxy to use. If user and password must be specified, add to the proxy URL.")

parser.add_option("-X", "--secure-proxy", action="store", dest="secure_proxy", type="string",
                  help="HTTPS proxy to use. If user and password must be specified, add to the proxy URL.")

(options, args) = parser.parse_args()

init = XInit(options.host_url, options.username, options.password, options.config, options.target, options.proxy,
             options.secure_proxy)
result = init.process()
status_ = result['status']
print 'Status:', status_
if status_ is not 200:
    print 'Reason: ' + result['reason']
